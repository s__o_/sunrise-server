from flask import Flask, request, jsonify
from sunrise.controller import cron
from sunrise.io import configuration as config

app = Flask(__name__)


@app.route("/sunrise", methods=["POST"])
def set_alarm():
    job = {
        "hour": None,
        "minute": None,
        "duration": None,
        "shutdown": None,
        "command": "echo Hello",
    }

    for arg in request.form:
        job[arg] = request.form[arg]

    cron.add_job(job)

    return jsonify({"status": 200, "alarm": job})


@app.route("/sunrise", methods=["GET"])
def get_alarm():
    if cron.exists("sunrise"):
        job = cron.get_job("sunrise")
        job_time = cron.time_from_job(job)

        return jsonify(
            {
                "status": 200,
                "time": job_time,
                "details": config.get("Power", "shutdown", "boolean"),
            }
        )
    else:
        return jsonify({"status": 404, "msg": "no alarm set"})


@app.route("/sunrise", methods=["DELETE"])
def delete_alarm():
    if cron.del_job("sunrise"):
        res = {"status": 200, "msg": "alarm deleted"}
    else:
        res = {"status": 404, "msg": "alarm not found"}
    return jsonify(res)


if __name__ == "__main__":
    app.run(debug=True)
