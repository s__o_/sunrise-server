def to_str(time):
    str_time = ""
    if time["h"] < 10:
        str_time += "".join(["0", time["h"]])
    else:
        str_time += str(time["h"])

    str_time += ":"

    if time["m"] < 10:
        str_time += "".join(["0", time["m"]])
    else:
        str_time += str(time["m"])

    return str_time


def time_diff(time, delta):
    pass
