import configparser as cp
from pathlib import Path

CONFIG_FILE = "settings.ini"


def get_config(file=CONFIG_FILE):
    parser = cp.ConfigParser()
    config_file = Path(file)

    if config_file.exists():
        parser.read(file)
    else:
        config_file = open(file, "w")
        parser.add_section("Alarm")
        parser.add_section("Power")

        parser.set("Power", "shutdown", "True")
        parser.set("Alarm", "duration", 30)
        parser.write(config_file)
        config_file.close()

    return parser


def set(section, key, value):
    conf = get_config()
    config_file = open(CONFIG_FILE, "w")
    conf.set(section, key, value)
    conf.write(config_file)
    config_file.close()
    # TODO: fill function


def get(section, key, type):
    conf = get_config()
    if type == "boolean":
        return conf[section].getboolean(key)
