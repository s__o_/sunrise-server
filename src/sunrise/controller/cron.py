from crontab import CronTab
import getpass


def add_job(job: dict, comment="sunrise"):
    user = getpass.getuser()
    manager = CronTab(user)

    if exists(comment):
        cron_job = get_job(comment)
        cron_job.hour.on(job["hour"])
        cron_job.minute.on(job["minute"])
    else:
        cron_job = manager.new(job["command"], comment)
        cron_job.hour.on(job["hour"])
        cron_job.minute.on(job["minute"])

    manager.write()


def del_job(key):
    user = getpass.getuser()
    manager = CronTab(user)

    for job in manager:
        if job.comment == key:
            manager.remove(job)
            manager.write()
            return True

    return False


def get_job(key):
    user = getpass.getuser()
    manager = CronTab(user)
    return list(filter(lambda job: job.comment == key, manager))[0]


def exists(key):
    user = getpass.getuser()
    manager = CronTab(user)
    return len(list(filter(lambda job: job.comment == key, manager))) > 0


def time_from_job(cron):
    return {"h": cron.hour.render(), "m": cron.minute.render()}
