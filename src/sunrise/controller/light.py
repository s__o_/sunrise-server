from yeelight import discover_bulbs, Bulb, Flow
from yeelight.transitions import pulse
import time

GREEN = (128, 224, 58)
RED = (231, 76, 60)


def discover():
    bulbs = discover_bulbs()
    return bulbs[0]["ip"]


def get_property(bulb, property):
    return bulb.get_properties()[property]


def get_light(attempts=5, wait=5):
    try:
        light_ip = discover()
        return Bulb(light_ip)
    except TimeoutError:
        for attemp in range(0, attempts):
            print("[controller/light] Try to reconnect ...")
            try:
                light_ip = discover()
                return Bulb(light_ip)
            except TimeoutError:
                time.sleep(wait)
        print("[controller/light] Unable to connect!")


def set_brightness(bulb, value, delta=False):
    if delta:
        current = float(get_property("bright"))
        bulb.set_brigthness(current + value)
    else:
        bulb.set_brightness(value)


def set_temp(bulb, value, delta=False):
    if delta:
        current = float(get_property("ct"))
        bulb.set_color_temp(current + value)
    else:
        bulb.set_color_temp(value)


def flow(bulb, color, duration=1500):
    bulb.turn_on()
    flow = Flow(
        1,
        Flow.actions.off,
        transitions=pulse(color[0], color[1], color[2], duration),
    )
    bulb.start_flow(flow)
