# Sunrise - Backend

This project provides the backend for the sunrise project. The sunrise project consists of an RGB Yeelight, this Flask API backend (deployed on a Raspberry Pi) and an [android application](https://gitlab.com/s__o_/sunrise-app).

All components work together as illustrated below:

![architecture](/pictures/architecture.png)

## Setup

For development:

```bash
pip install -e .
```

This will start the Flask server, which will run on `http://127.0.0.1:5000/`
